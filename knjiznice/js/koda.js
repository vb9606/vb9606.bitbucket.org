
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function calculateBMI() {
    var heigth= document.getElementById("heigth").value;
    var weigth = document.getElementById("weigth").value;
    
    if(heigth.length == 0) {
        document.getElementById("heigth").value="Izpolnite okence!";
    }
    if(weigth.length == 0) {
        document.getElementById("weigth").value="Izpolnite okence!";
    }
    
    heigth = heigth/100;
    
    var BMI = weigth/(heigth*heigth);
    
    if ( $("heigth").trim == 0) {
        document.getElementById("BMIcalculate").value = "Vnesite vse potrebne podatke";
    }
    
    if ( $("weigth").trim == 0) {
        document.getElementById("BMIcalculate").value = "Vnesite vse potrebne podatke";
    }
    
   
    
    if(BMI < 18.5){
        document.getElementById("BMIcalculate").value = BMI + " ,prenizek BMI, obiščite doktorja!";
    }
    
    if(BMI >= 18.5 && BMI <= 24.9) {
        document.getElementById("BMIcalculate").value = BMI + " ,normalen BMI, kar tako naprej!";
    }
    
    if(BMI >= 25 && BMI <= 29.9) {
        document.getElementById("BMIcalculate").value = BMI + " ,malo previsok BMI, morate shujšati!";
    }
    
    if(BMI > 30) {
        document.getElementById("BMIcalculate").value = BMI + " ,zelo visok BMI, obiščite doktorja!";
    }
    
}

function checkIfFever() {
    var fever = document.getElementById("bodyTemperature").value;
    
    if ( $("fever").trim == 0) {
        document.getElementById("ifFever").value = "Vnesite vse potrebne podatke";
    }
    
    if(fever < 35.8) {
        document.getElementById("ifFever").value = fever + " °C , imate prenizko telesno temperaturo, obiščite doktorja!";
    }
    
    else if(fever >= 35.8 && fever < 37) {
        document.getElementById("ifFever").value = fever + " °C , imate normalno telesno temperaturo!";
    }
    
    else if(fever >= 37 && fever < 37.5) {
        document.getElementById("ifFever").value = fever + " °C , imate rahlo zvišano telesno temperaturo, dobro se oblecite v mrzlem okolju!";
    }
    
    else if(fever >= 37.5 && fever < 40) {
        document.getElementById("ifFever").value = fever + " °C , imate vročino, ostanite doma in pijte čaj!";
    }
    
    else if(fever >= 40) {
        document.getElementById("ifFever").value = fever + ", imate visoko vročino, obiščite doktorja!";
    }
}
function checkSPressure() {
    var sPressure = document.getElementById("systolicPressure").value;
    
    if ( $("sPressure").trim == 0) {
        document.getElementById("whatSPressure").value = "Vnesite vse potrebne podatke";
    }
    
    if(sPressure < 110) {
        document.getElementById("whatSPressure").value = sPressure + " mmHg , imate prenizek sistolični krvni tlak, obiščite doktorja!";
    }
    
    if(sPressure >= 110 && sPressure < 140) {
        document.getElementById("whatSPressure").value = sPressure + " mmHg , imate normalen sistolični krvni tlak!";
    }
    
    if(sPressure > 140) {
        document.getElementById("whatSPressure").value = sPressure + " mmHg , imate previsok sistolični krvni tlak, obiščite doktorja!";
    }
}

function checkDPressure() {
    var dPressure = document.getElementById("diastolicPressure").value;
    
    if ( $("dPressure").trim == 0) {
        document.getElementById("whatDPressure").value = "Vnesite vse potrebne podatke";
    }
    
    if(dPressure < 60) {
        document.getElementById("whatDPressure").value = dPressure + " mmHg , imate prenizek diastolični krvni tlak, obiščite doktorja!";
    }
    
    if(dPressure >= 60 && dPressure < 90) {
        document.getElementById("whatDPressure").value = dPressure + " mmHg , imate normalen diastolični krvni tlak!";
    }
    
    if(dPressure > 90) {
        document.getElementById("whatDPressure").value = dPressure + " mmHg , imate previsok diastolični krvni tlak, obiščite doktorja!";
    }
}

function createEHR() {
	sessionId = getSessionId();

	var ime = document.getElementById("ime").value;
	var priimek = document.getElementById("priimek").value;
	var datumRojstva = document.getElementById("datumRojstva").value;

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		alert("Niste vnesli vseh podatkov!");
		return;
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                   alert("EHR id:" + ehrId);
		                   return;
		                }
		            },
		            error: function(err) {
		            	alert(JSON.parse(err.responseText).userMessage+ "'!");
		            	return;
		            }
		        });
		    }
		});
	}
}

function readEHR() {
	sessionId = getSessionId();

	var ehrId = document.getElementById("EHRId").value;
	

	if (!ehrId || ehrId.trim().length == 0) {
		alert("Če želite shraniti podakte, vnesite svoj EHR id!");
		return;
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
                alert("Bolnik" + party.firstNames + " " + party.lastNames + " , ki se je rodil" + party.dateOfBirth + ".");
                return;
			}, 
			error: function(err) {
          alert(JSON.parse(err.responseText).userMessage + "'!");
          return;
			}
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#EHRId").val();
	var telesnaVisina = $("#heigth").val();
	var telesnaTeza = $("#weigth").val();
	var telesnaTemperatura = $("#bodyTemperature").val();
	var sistolicniKrvniTlak = $("#systolicPressure").val();
	var diastolicniKrvniTlak = $("#diastolicPressure").val();

	if (!ehrId || ehrId.trim().length == 0) {
		alert("Prosim vnesite zahtevane podatke!");
		return;
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = ocument.getElementById("yoursEHRId").value;
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		alert("Prosim vnesite zahtevan podatek!");
		return;
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].temperature +
                          " " + res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		alert("Ni podatkov!");
					    		return;
					    	}
					    },
					    error: function() {
					    	alert(JSON.parse(err.responseText).userMessage + "'!");
					    	return;
					    }
					});
				} else if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		alert("Ni podatkov!");
					    		return;
					    	}
					    },
					    error: function() {
					    	alert(JSON.parse(err.responseText).userMessage + "'!");
					    	return;
					    }
					});
				} else if (tip == "telesna temperatura AQL") {
					var AQL =
						"select " +
    						"t/data[at0002]/events[at0003]/time/value as cas, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as temperatura_vrednost, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as temperatura_enota " +
						"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
						"contains OBSERVATION t[openEHR-EHR-OBSERVATION.body_temperature.v1] " +
						"where t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude<35 " +
						"order by t/data[at0002]/events[at0003]/time/value desc " +
						"limit 10";
					$.ajax({
					    url: baseUrl + "/query?" + $.param({"aql": AQL}),
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var results = "<table class='table table-striped table-hover'>" +
                  "<tr><th>Datum in ura</th><th class='text-right'>" +
                  "Telesna temperatura</th></tr>";
					    	if (res) {
					    		var rows = res.resultSet;
						        for (var i in rows) {
						            results += "<tr><td>" + rows[i].cas +
                          "</td><td class='text-right'>" +
                          rows[i].temperatura_vrednost + " " 	+
                          rows[i].temperatura_enota + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    	    alert("Ni podatkov!");
					    		    return;
					    	}

					    },
					    error: function() {
					    	alert(JSON.parse(err.responseText).userMessage + "'!");
					    	return;
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		alert(JSON.parse(err.responseText).userMessage + "'!");
	    		return;
	    	}
		});
	}
}

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#ime").val(podatki[0]);
    $("#priimek").val(podatki[1]);
    $("#datumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#yoursEHRId").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
